## Authors
Biomatters Ltd

## Summary
A Geneious plugin which adds two operations to reverse a DNA sequence (without complementing) and to complement a DNA sequence (without reversing)

## Installation
Download the gplugin file from http://geneious.com/plugins and drag it into Geneious.